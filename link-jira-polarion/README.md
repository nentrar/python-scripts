
The instruction covers the **manual of using the custom made script to automate linking Polarion workitems to Jira issues.**

# Requirements

To be able to execute the script, user have to **fulfill the following requirements:**

-   have latest **Python installed** on the PC along with **pip** and possible also **virtualenv**,
    
-   have **GIT installed** and configured on the PC,
    
-   have **account for both Jira and Polarion**,
    
-   have a **list of Polarion workitems** that she want to be linked to the related Jira issues.
    

## Install Python and GIT

This instruction would not cover the above requirement but I insert the helpful sites which could help in that matter:

1.  [![](https://www.python.org/static/favicon.ico)Download Python](https://www.python.org/downloads/)
    
2.  [![](https://pip.pypa.io/favicon.ico)Installation - pip documentation v23.3.1](https://pip.pypa.io/en/stable/installation/)
    
3.  [![](https://virtualenv.pypa.io/favicon.ico)Installation - virtualenv](https://virtualenv.pypa.io/en/latest/installation.html)
    
4.  [![](https://git-scm.com/favicon.ico)Git - Installing Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
    

# Setup script

Once all the required dependencies are succesfully installed, we can **start with setup the repository** and needed configuration files. Start with cloning the repository and when it's finished, go into the directory.  

## Install the required packages

When installing the required Python packages it is a good practice to use a virtualenv. The only **bottleneck is that it need to be activated each time** when you want to run the Script. Therefore **you can skip this part and go straight to installing the requirements** if you want.

To create virtualenv, type in the terminal the following command:

    `python3 -m virtualenv venv`

When the venv directory is created, activate the virtualenv with the following:

    `./venv/Scripts/activate`

Note that above command would only work on Windows operating system. When using Linux, use the following:

    source venv/bin/activate

With virtualenv activated we can install the required packages using the given requirements file. Enter the following command and wait for all packages to be downloaded.

    `pip install -r requirements.txt`

Now everything is prepared for running the Script but we still need to configure credentials and Polarion data.

## Prepare credentials

The script is using Python libraries for both Jira and Polarion which connect to these platforms using the API. To successfully establish the connection we need right usernames and tokens.

### Polarion

The input for the `POLARION_USER` should be your DeLaval username. As for the `POLARION_TOKEN` you need to log into the Polarion and then **click on the cog icon to enter My account page**.
There on the header there is a Personal Access Token where You can generate a new token.

**Give it a name** to be able to distinguish token from each other and expiration date for security purposes. Next **click on Create Token** and copy given string to the clipboard. Insert it into the `POLARION_TOKEN` in the .env file.

### Jira

Now to prepare a credentials for Jira, you must enter your email into the `JIRA_USER` . And for the `JIRA_TOKEN` follow the below instruction. Log into the Jira platform and **click on the User icon to enter the Manage account** page.

Now in the API tokens section enter the **Create and manage API tokens.**
Click on Create API token, name it whatever you like and save the given string into the `JIRA_TOKEN` in the .env file.

## Prepare Polarion workitems

**The script will only work if the given workitems have the custom JiraID label filled**. Keep this in mind when inserting the IDs. The .json data format is the following:

    `{  "SY113":  ["SY113-1696",  "SY113-1697"],  "PD126":  ["PD126-1788",  "PD126-1789"]  }`

In the first string **enter the project ID** following by the **selected Polarion workitems**, each divided by the comma. And thats it. With the configuration done we can start the script and wait for the results.

# Execute the script

**To execute the script**, still in the Terminal and in the project repository, **enter the following command:**

    python .\link.py

The linking process will start and the **logging will be put into the console** with the results.


Then when we enter the **Jira issue, we can see the weblink** added to the Polarion issue and the same for the **Polarion issue - with the hyperlink** to the Jira issue.

With the script executed, **all selected Polarion workitems are linked with the corresponding to them Jira issues** and can be easly accessed in the respected platforms without the need of searching.