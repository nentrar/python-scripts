from polarion import polarion
from jira import JIRA
from dotenv import load_dotenv
import os, json, logging


class Link:

    def __init__(self) -> None:

        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)
        
        load_dotenv()
        logging.info("Loading environmental variables...")

        self.polarion_url = os.getenv("POLARION_URL")
        self.polarion_user = os.getenv("POLARION_USER")
        self.polarion_token = os.getenv("POLARION_TOKEN")

        self.jira_url = os.getenv("JIRA_URL")
        self.jira_server = {'server': self.jira_url}
        self.jira_user = os.getenv("JIRA_USER")
        self.jira_token = os.getenv("JIRA_TOKEN")

        logging.info("All variables loaded.")

        logging.info("Setup platform clients...")

        try:
            self.polarion_client = polarion.Polarion(self.polarion_url, self.polarion_user, None, self.polarion_token)
            logging.info("Polarion connection established successfully.")
        except:
            logging.error("Polarion connection cannot be established. Check credentials or VPN.")

        try:
            self.jira_client = JIRA(options=self.jira_server, basic_auth=(self.jira_user, self.jira_token))
            logging.info("JIRA connection established successfull.")
        except:
            logging.error("JIRA connection cannot be established. Check credentials or VPN.")

        logging.info("Platform clients connection is established successfully.")

        logging.info("Load workitem data...")
        with open("polarion_data.json") as data:
            self.workitems = json.load(data)
        logging.info(f'The following data is loaded and prepared for linking: {self.workitems}')
    
    def prepare_jira_id_value(self, polarion_project, polarion_id):

        project = self.polarion_client.getProject(polarion_project)
        workitem = project.getWorkitem(polarion_id)
        jira_id_value = None

        for item in workitem.customFields['Custom']:
            if item['key'] == 'JIRAid':
                jira_id_value = item['value']
                break

        return jira_id_value
    
    def add_polarion_link_to_jira(self, jira_id, polarion_id, polarion_project):

        polarion_link = f'{self.polarion_url}/#/project/{polarion_project}/workitem?id={polarion_id}'

        issue = self.jira_client.issue(jira_id)

        self.jira_client.add_simple_link(issue, {"url": polarion_link, "title": f'Polarion issue {polarion_id}'})

    def add_jira_link_to_polarion(self, jira_id, polarion_id, polarion_project):
        
        jira_link = f'{self.jira_url}/browse/{jira_id}'

        workitem = self.polarion_client.getProject(polarion_project).getWorkitem(polarion_id)

        workitem.addHyperlink(jira_link, hyperlink_type="external")

    def link_issues(self):

        for project in self.workitems:

            logging.info(f'Starting linking items in the {project} Polarion project.')

            for item in range(len(self.workitems[project])):

                logging.info(f'Open {self.workitems[project][item]} issue.')

                try:    
                    jira_id = self.prepare_jira_id_value(project, self.workitems[project][item])
                    logging.info(f'{jira_id} issue taken from the label JiraID in Polarion')
                except:
                    logging.error("JIRA issue data cannot be taken from the JiraID label in Polarion. Check if the label exist.")

                logging.info(f'Start linking Polarion issue {self.workitems[project][item]} back to Jira issue {jira_id}.')

                try:
                    self.add_polarion_link_to_jira(jira_id, self.workitems[project][item], project)
                    logging.info("Polarion link was successfully added to Jira as a weblink.")
                except:
                    logging.error("Polarion issue cannot be added into Jira as a weblink. Check the connection and try again.")

                logging.info(f'Start linking Jira issue {jira_id} back to Polarion issue {self.workitems[project][item]}.')

                try:
                    self.add_jira_link_to_polarion(jira_id, self.workitems[project][item], project)
                    logging.info("Jira link was successfully added to Polarion as a hyperlink.")
                except:
                    logging.error("Jira issue cannot be added into Polarion as a hyperlink. Check the connection and try again.")

                logging.info(f'Polarion workitem {self.workitems[project][item]} was successfully linked with the Jira issue {jira_id}')   

            logging.info(f'All workitems related to {project} project and specified in the JSON data were successfully linked.')             

           
new = Link()
new.link_issues()